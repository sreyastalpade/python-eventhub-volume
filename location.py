# # packages required
# azure-eventhub == 5.4.0
# azure-eventhub-checkpointstoreblob-aio == 1.1.4

import requests
import asyncio
from azure.eventhub.aio import EventHubConsumerClient
from azure.eventhub.extensions.checkpointstoreblobaio import BlobCheckpointStore

CONNECTION_STRING = "Endpoint=sb://ehdltls.servicebus.windows.net/;SharedAccessKeyName=EHConsumerLineHaul;SharedAccessKey=OG2Jif/M6fD3JcQSDfYmRUqRylxt6rilqIDuU5doVOA="
EVENTHUB_NAME = "prod-tls-se-location"
STORAGE_CONNECTION_STRING = "DefaultEndpointsProtocol=https;AccountName=neweventstorageaccount;AccountKey=ROzxD8IKBtP6XDTCz5frLwOffyIRtbu+ZpOCVSHbFuyfmkm9fdwOC9RfU4GzOLS6dGFIOpSRpWdsL+IbRR0Smw==;EndpointSuffix=core.windows.net"
BLOB_CONTAINER_NAME = "test-py"
CONSUMER_GROUP = "ehconsumer-lh"
HOST_NAME = "10.0.137.184"
URL = "http://" + HOST_NAME + "/sendLocation"

# receiving data from eventhub
async def on_event(partition_context, event):
    # Print the event data with respose status.
    print("Posting to: {}, event: \"{}\"".format(URL, event.body_as_json(encoding='UTF-8')))
    r = requests.post(URL, json = event.body_as_json(encoding='UTF-8'))
    print("status_code: {}, reason: {}".format(r.status_code, r.reason))

    # Update the checkpoint so that the program doesn't read the events
    # that it has already read when you run it next time.
    await partition_context.update_checkpoint(event)

async def main():
    # Create an Azure blob checkpoint store to store the checkpoints.
    checkpoint_store = BlobCheckpointStore.from_connection_string(STORAGE_CONNECTION_STRING, BLOB_CONTAINER_NAME)

    # Create a consumer client for the event hub.
    client = EventHubConsumerClient.from_connection_string(CONNECTION_STRING, consumer_group=CONSUMER_GROUP, eventhub_name=EVENTHUB_NAME, checkpoint_store=checkpoint_store)
    async with client:
        # Call the receive method. Read from the beginning of the partition (starting_position: "-1")
        await client.receive(on_event=on_event,  starting_position="-1")

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    # Run the main method.
    loop.run_until_complete(main())